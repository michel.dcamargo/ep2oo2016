package view;

import controller.Principal;
import static controller.Funcoes.le_linha;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.LayoutManager;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import model.ImagemPGM;

public class PGMReader extends javax.swing.JFrame {    
    private ImagemPGM imagemOriginal = new ImagemPGM();
    private ImagemPGM imagemAlterada = new ImagemPGM();
    private JFrame exibeImagem;
    
    public PGMReader() {
        initComponents();
        setVisible(true);
        setEnabled(true);
    }
   
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        labelInfo = new javax.swing.JLabel();
        jbDecifrar = new javax.swing.JButton();
        jbSmooth = new javax.swing.JButton();
        jbNegativo = new javax.swing.JButton();
        jbSharpen = new javax.swing.JButton();
        labelFiltros = new javax.swing.JLabel();
        labelDecifrar = new javax.swing.JLabel();
        tfendereco = new javax.swing.JTextField();
        jbPesquisar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("ImagemPGM");
        setEnabled(false);
        setResizable(false);
        setSize(new java.awt.Dimension(829, 549));

        labelInfo.setFont(new java.awt.Font("Ubuntu", 1, 15)); // NOI18N
        labelInfo.setText("Digite o caminho da imagem: ");

        jbDecifrar.setText("Decifrar txt");
        jbDecifrar.setEnabled(false);
        jbDecifrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbDecifrarActionPerformed(evt);
            }
        });

        jbSmooth.setText("Smooth");
        jbSmooth.setEnabled(false);
        jbSmooth.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbSmoothActionPerformed(evt);
            }
        });

        jbNegativo.setText("Negativo");
        jbNegativo.setEnabled(false);
        jbNegativo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbNegativoActionPerformed(evt);
            }
        });

        jbSharpen.setText("Sharpen");
        jbSharpen.setEnabled(false);
        jbSharpen.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbSharpenActionPerformed(evt);
            }
        });

        labelFiltros.setText("Filtros:");

        labelDecifrar.setText("Decifrar mensagem escondida:");

        tfendereco.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tfenderecoActionPerformed(evt);
            }
        });
        tfendereco.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                tfenderecoKeyTyped(evt);
            }
        });

        jbPesquisar.setText("Pesquisar");
        jbPesquisar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbPesquisarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jbNegativo)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jbSharpen, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jbSmooth, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(labelFiltros))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 356, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(labelDecifrar, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jbDecifrar, javax.swing.GroupLayout.Alignment.TRAILING)))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(labelInfo)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(tfendereco)
                        .addGap(18, 18, 18)
                        .addComponent(jbPesquisar)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(13, 13, 13)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelInfo)
                    .addComponent(tfendereco, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jbPesquisar))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelFiltros)
                    .addComponent(labelDecifrar))
                .addGap(12, 12, 12)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jbNegativo)
                    .addComponent(jbDecifrar)
                    .addComponent(jbSharpen)
                    .addComponent(jbSmooth))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jbDecifrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbDecifrarActionPerformed
        byte[] pix = imagemOriginal.getPixels();
        byte bb = 0;                
        int j = imagemOriginal.getPosInicial();
        StringBuilder texto = new StringBuilder();
        
        System.out.println("TAmanho: \n" + pix.toString());
        while(true){
            for (int i = 1; i <= 8; i++) {
                bb = (byte) (pix[j] & 0x01);
                bb = (byte) ((bb << 1) | bb);
                j++;
            }
            if((char) bb == '#') 
                break;
            texto.append((char) bb);
        }
        
        JOptionPane.showMessageDialog(null, texto.toString()); 
    }//GEN-LAST:event_jbDecifrarActionPerformed

    private void jbSharpenActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbSharpenActionPerformed
        BufferedImage imagem_pgm = null, imagem_pgm_negativo = null;
        int count = 0, i, j, value;
        int [] filter = {0,1,0,1,5,1,0,1,0};
        
        imagem_pgm = new BufferedImage(imagemOriginal.getWidth(), imagemOriginal.getHeight(), BufferedImage.TYPE_BYTE_GRAY);
        imagem_pgm_negativo = new BufferedImage(imagemOriginal.getWidth(), imagemOriginal.getHeight(), BufferedImage.TYPE_BYTE_GRAY);
        
        byte[] pixels = ((DataBufferByte) imagem_pgm.getRaster().getDataBuffer()).getData();
        byte[] pixels_neg = ((DataBufferByte) imagem_pgm_negativo.getRaster().getDataBuffer()).getData();
        
        byte[] pix = imagemOriginal.getPixels();
        System.out.println("pegou pixels\n"); 
//        while(count < (imagemOriginal.getHeight() * imagemOriginal.getWidth())) {
//            pixels[count] = (byte) pix[count];
//            pixels_neg[count] = (byte) (imagemOriginal.getMaxVal() - pix[count]); 
//            count++;  
//	}
        for(i = 1; i < imagemOriginal.getWidth() - 1; i++){
            for (j = 1; j < imagemOriginal.getHeight() -1; i++){
                value = 0;
                for(int x = -1; x <= 1; x++){
                    for(int y = -1; y <= 1; y++){
                        value += filter[(x+1) + 3 + (y+1)] * pix[(i+x) + (y+j) * imagemOriginal.getWidth()];
                    }
                }
            value /= 1;
            value = value < 0 ? 0 : value;
            value = value > 255 ? 255 : value;
            pixels_neg[(i + j) * imagemOriginal.getWidth()] = (byte) value;
            }
        }

        System.out.println("Saindo do loop!\n"); 
        exibeImagem = new JFrame();
        exibeImagem.getContentPane().setLayout(new FlowLayout());
        exibeImagem.getContentPane().add(new JLabel(new ImageIcon(imagem_pgm)));
        exibeImagem.getContentPane().add(new JLabel(new ImageIcon(imagem_pgm_negativo)));
        exibeImagem.setMaximumSize(new Dimension(829,500));
        exibeImagem.pack();
        exibeImagem.setVisible(true);
    }//GEN-LAST:event_jbSharpenActionPerformed

    private void jbSmoothActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbSmoothActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jbSmoothActionPerformed

    private void jbNegativoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbNegativoActionPerformed
        BufferedImage imagem_pgm = null, imagem_pgm_negativo = null;
        int count = 0;
        
        imagem_pgm = new BufferedImage(imagemOriginal.getWidth(), imagemOriginal.getHeight(), BufferedImage.TYPE_BYTE_GRAY);
        imagem_pgm_negativo = new BufferedImage(imagemOriginal.getWidth(), imagemOriginal.getHeight(), BufferedImage.TYPE_BYTE_GRAY);
        
        byte[] pixels = ((DataBufferByte) imagem_pgm.getRaster().getDataBuffer()).getData();
        byte[] pixels_neg = ((DataBufferByte) imagem_pgm_negativo.getRaster().getDataBuffer()).getData();
        
        byte[] pix = imagemOriginal.getPixels();
        System.out.println("pegou pixels\n"); 
        while(count < (imagemOriginal.getHeight() * imagemOriginal.getWidth())) {
            pixels[count] = (byte) pix[count];
            pixels_neg[count] = (byte) (imagemOriginal.getMaxVal() - pix[count]); 
            count++;  
	}
        System.out.println("Saindo do loop!\n"); 
        exibeImagem = new JFrame();
        exibeImagem.getContentPane().setLayout(new FlowLayout());
        exibeImagem.getContentPane().add(new JLabel(new ImageIcon(imagem_pgm)));
        exibeImagem.getContentPane().add(new JLabel(new ImageIcon(imagem_pgm_negativo)));
        exibeImagem.setMaximumSize(new Dimension(829,500));
        exibeImagem.pack();
        exibeImagem.setVisible(true);
    }//GEN-LAST:event_jbNegativoActionPerformed

    private void tfenderecoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tfenderecoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_tfenderecoActionPerformed

    private void jbPesquisarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbPesquisarActionPerformed
        try {
            FileInputStream arquivo = new FileInputStream(tfendereco.getText());
            BufferedImage imagem_pgm = null;
            int count = 0;
            byte bb;
            
            String linha = le_linha(arquivo);
            if ("P5".equals(linha)) {
                linha = le_linha(arquivo);
                while (linha.startsWith("#")) {
                    Pattern p = Pattern.compile("[0-9]+");
                    Matcher m = p.matcher(linha);
                    StringBuilder nroExtraidos = new StringBuilder();
                    while (m.find()) {
                        nroExtraidos.append(m.group().trim());
                    }
                    imagemOriginal.setPosInicial(Integer.parseInt(nroExtraidos.toString()));
                    System.out.println("Pos inicial: \n" + imagemOriginal.getPosInicial());
                    linha = le_linha(arquivo);
                }
                Scanner in = new Scanner(linha);
                if (in.hasNext() && in.hasNextInt()) {
                    imagemOriginal.setWidth(in.nextInt());
                } else {
                    System.out.println("Arquivo corrompido");
                }
                if (in.hasNext() && in.hasNextInt()) {
                    imagemOriginal.setHeight(in.nextInt());
                } else {
                    System.out.println("Arquivo corrompido");
                }
                linha = le_linha(arquivo);
                in.close();
                in = new Scanner(linha);
                imagemOriginal.setMaxVal(in.nextInt());
                in.close();

                imagem_pgm = new BufferedImage(imagemOriginal.getWidth(), imagemOriginal.getHeight(), BufferedImage.TYPE_BYTE_GRAY);

                byte[] pixels = ((DataBufferByte) imagem_pgm.getRaster().getDataBuffer()).getData();
                
                System.out.println("Entrando no loop...\n");                
                while (count < (imagemOriginal.getHeight() * imagemOriginal.getWidth())) {
                    bb = (byte) arquivo.read();
                    pixels[count] = bb;
                    count++;
                }
                imagemOriginal.setPixels(pixels);
                System.out.println("Saindo do loop!\n");                
            } else {
                System.out.println("Arquivo inválido");
            }
            System.out.println("Imprimindo\n");
            
//            exibeImagem = new JFrame();
//            exibeImagem.getContentPane().setLayout(new FlowLayout());
//            exibeImagem.getContentPane().add(new JLabel(new ImageIcon(imagem_pgm)));
//            exibeImagem.pack();
//            exibeImagem.setVisible(true);
            
            JOptionPane.showMessageDialog(null, "Arquivo encontrado em: \n" + tfendereco.getText());
            jbDecifrar.setEnabled(true);
//            jbSharpen.setEnabled(true);
//            jbSmooth.setEnabled(true);
            jbNegativo.setEnabled(true);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(PGMReader.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, "Arquivo não encontrado em: \n" + tfendereco.getText());
        } catch (IOException ex) {
            Logger.getLogger(PGMReader.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, "Erro na leitura do arquivo.");
        }
    }//GEN-LAST:event_jbPesquisarActionPerformed

    private void tfenderecoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tfenderecoKeyTyped
        if(evt.getKeyChar() == '\n')
            jbPesquisar.doClick(2);
    }//GEN-LAST:event_tfenderecoKeyTyped

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jbDecifrar;
    private javax.swing.JButton jbNegativo;
    private javax.swing.JButton jbPesquisar;
    private javax.swing.JButton jbSharpen;
    private javax.swing.JButton jbSmooth;
    private javax.swing.JLabel labelDecifrar;
    private javax.swing.JLabel labelFiltros;
    private javax.swing.JLabel labelInfo;
    private javax.swing.JTextField tfendereco;
    // End of variables declaration//GEN-END:variables
}
