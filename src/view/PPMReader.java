package view;

import static controller.Funcoes.le_linha;
import java.awt.Dimension;
import java.awt.Event;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import model.ImagemPPM;
import java.awt.event.KeyEvent;

public class PPMReader extends javax.swing.JFrame { 
    private ImagemPPM imagemOriginal = new ImagemPPM();
    private ImagemPPM imagemAlterada = new ImagemPPM();
    private JFrame exibeImagem;
    
    public PPMReader() {
        initComponents();
        setVisible(true);
        setEnabled(true);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        labelInfo = new javax.swing.JLabel();
        labelFiltrosRGB = new javax.swing.JLabel();
        jbRED = new javax.swing.JButton();
        jbGREEN = new javax.swing.JButton();
        jbBLUE = new javax.swing.JButton();
        jbNegativo = new javax.swing.JButton();
        labelNegativo = new javax.swing.JLabel();
        jbPesquisar = new javax.swing.JButton();
        tfendereco = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("ImagemPPM");
        setResizable(false);
        setSize(new java.awt.Dimension(829, 549));

        labelInfo.setFont(new java.awt.Font("Ubuntu", 1, 15)); // NOI18N
        labelInfo.setText("Digite o caminho da imagem: ");

        labelFiltrosRGB.setText("Filtros RGB:");

        jbRED.setText("RED");
        jbRED.setEnabled(false);
        jbRED.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbREDActionPerformed(evt);
            }
        });

        jbGREEN.setText("GREEN");
        jbGREEN.setEnabled(false);
        jbGREEN.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbGREENActionPerformed(evt);
            }
        });

        jbBLUE.setText("BLUE");
        jbBLUE.setEnabled(false);
        jbBLUE.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbBLUEActionPerformed(evt);
            }
        });

        jbNegativo.setText("Negativo");
        jbNegativo.setEnabled(false);
        jbNegativo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbNegativoActionPerformed(evt);
            }
        });

        labelNegativo.setText("Filtro Negativo:");

        jbPesquisar.setText("Pesquisar");
        jbPesquisar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbPesquisarActionPerformed(evt);
            }
        });

        tfendereco.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                tfenderecoKeyTyped(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jbRED, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jbGREEN, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jbBLUE, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jbNegativo))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(labelFiltrosRGB)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(labelNegativo))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(labelInfo)
                        .addGap(9, 9, 9)
                        .addComponent(tfendereco, javax.swing.GroupLayout.DEFAULT_SIZE, 488, Short.MAX_VALUE)
                        .addGap(18, 18, 18)
                        .addComponent(jbPesquisar)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(8, 8, 8)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(tfendereco, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jbPesquisar))
                    .addComponent(labelInfo))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelNegativo)
                    .addComponent(labelFiltrosRGB))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jbNegativo)
                    .addComponent(jbBLUE)
                    .addComponent(jbGREEN)
                    .addComponent(jbRED))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jbREDActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbREDActionPerformed
        BufferedImage imagem_ppm = null, imagem_ppm_red = null;
        int count = 0, count2 = 0;
        
        imagem_ppm = new BufferedImage(imagemOriginal.getWidth(), imagemOriginal.getHeight(), BufferedImage.TYPE_3BYTE_BGR);
        imagem_ppm_red = new BufferedImage(imagemOriginal.getWidth(), imagemOriginal.getHeight(), BufferedImage.TYPE_3BYTE_BGR);
        
        byte[] pixels = ((DataBufferByte) imagem_ppm.getRaster().getDataBuffer()).getData();
        byte[] pixels_red = ((DataBufferByte) imagem_ppm_red.getRaster().getDataBuffer()).getData();
        
        byte[] pix = imagemOriginal.getPixels();
        System.out.println("pegou pixels\n"); 
        while(count < (imagemOriginal.getHeight() * imagemOriginal.getWidth())) {
            pixels[count2] = (byte) pix[count2];
            pixels_red[count2] = (byte) 0; 
            count2++;
            pixels[count2] = (byte) pix[count2];
            pixels_red[count2] = (byte) 0; 
            count2++;
            pixels[count2] = (byte) pix[count2];
            pixels_red[count2] = (byte) pix[count2]; 
            count2++;            
            count++;  
	}
        System.out.println("Saindo do loop!\n"); 
        exibeImagem = new JFrame();
        exibeImagem.getContentPane().setLayout(new GridLayout(2,1));
        exibeImagem.getContentPane().add(new JLabel(new ImageIcon(imagem_ppm)));
        exibeImagem.getContentPane().add(new JLabel(new ImageIcon(imagem_ppm_red)));
        exibeImagem.setMaximumSize(new Dimension(829,1000));
        exibeImagem.pack();
        exibeImagem.setVisible(true);
    }//GEN-LAST:event_jbREDActionPerformed

    private void jbGREENActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbGREENActionPerformed
        BufferedImage imagem_ppm = null, imagem_ppm_green = null;
        int count = 0, count2 = 0;
        
        imagem_ppm = new BufferedImage(imagemOriginal.getWidth(), imagemOriginal.getHeight(), BufferedImage.TYPE_3BYTE_BGR);
        imagem_ppm_green = new BufferedImage(imagemOriginal.getWidth(), imagemOriginal.getHeight(), BufferedImage.TYPE_3BYTE_BGR);
        
        byte[] pixels = ((DataBufferByte) imagem_ppm.getRaster().getDataBuffer()).getData();
        byte[] pixels_green = ((DataBufferByte) imagem_ppm_green.getRaster().getDataBuffer()).getData();
        
        byte[] pix = imagemOriginal.getPixels();
        System.out.println("pegou pixels\n"); 
        while(count < (imagemOriginal.getHeight() * imagemOriginal.getWidth())) {
            pixels[count2] = (byte) pix[count2];
            pixels_green[count2] = (byte) 0; 
            count2++;
            pixels[count2] = (byte) pix[count2];
            pixels_green[count2] = (byte) pix[count2]; 
            count2++;
            pixels[count2] = (byte) pix[count2];
            pixels_green[count2] = (byte) 0; 
            count2++;            
            count++;  
	}
        System.out.println("Saindo do loop!\n"); 
        exibeImagem = new JFrame();
        exibeImagem.getContentPane().setLayout(new GridLayout(2,1));
        exibeImagem.getContentPane().add(new JLabel(new ImageIcon(imagem_ppm)));
        exibeImagem.getContentPane().add(new JLabel(new ImageIcon(imagem_ppm_green)));
        exibeImagem.setMaximumSize(new Dimension(829,1000));
        exibeImagem.pack();
        exibeImagem.setVisible(true);
    }//GEN-LAST:event_jbGREENActionPerformed

    private void jbBLUEActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbBLUEActionPerformed
                BufferedImage imagem_ppm = null, imagem_ppm_blue = null;
        int count = 0, count2 = 0;
        
        imagem_ppm = new BufferedImage(imagemOriginal.getWidth(), imagemOriginal.getHeight(), BufferedImage.TYPE_3BYTE_BGR);
        imagem_ppm_blue = new BufferedImage(imagemOriginal.getWidth(), imagemOriginal.getHeight(), BufferedImage.TYPE_3BYTE_BGR);
        
        byte[] pixels = ((DataBufferByte) imagem_ppm.getRaster().getDataBuffer()).getData();
        byte[] pixels_blue = ((DataBufferByte) imagem_ppm_blue.getRaster().getDataBuffer()).getData();
        
        byte[] pix = imagemOriginal.getPixels();
        System.out.println("pegou pixels\n"); 
        while(count < (imagemOriginal.getHeight() * imagemOriginal.getWidth())) {
            pixels[count2] = (byte) pix[count2];
            pixels_blue[count2] = (byte) pix[count2]; 
            count2++;
            pixels[count2] = (byte) pix[count2];
            pixels_blue[count2] = (byte) 0; 
            count2++;
            pixels[count2] = (byte) pix[count2];
            pixels_blue[count2] = (byte) 0; 
            count2++;            
            count++;  
	}
        System.out.println("Saindo do loop!\n"); 
        exibeImagem = new JFrame();
        exibeImagem.getContentPane().setLayout(new GridLayout(2,1));
        exibeImagem.getContentPane().add(new JLabel(new ImageIcon(imagem_ppm)));
        exibeImagem.getContentPane().add(new JLabel(new ImageIcon(imagem_ppm_blue)));
        exibeImagem.setMaximumSize(new Dimension(829,1000));
        exibeImagem.pack();
        exibeImagem.setVisible(true);
    }//GEN-LAST:event_jbBLUEActionPerformed

    private void jbNegativoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbNegativoActionPerformed
        BufferedImage imagem_ppm = null, imagem_ppm_negativo = null;
        int count = 0;
        
        imagem_ppm = new BufferedImage(imagemOriginal.getWidth(), imagemOriginal.getHeight(), BufferedImage.TYPE_3BYTE_BGR);
        imagem_ppm_negativo = new BufferedImage(imagemOriginal.getWidth(), imagemOriginal.getHeight(), BufferedImage.TYPE_3BYTE_BGR);
        
        byte[] pixels = ((DataBufferByte) imagem_ppm.getRaster().getDataBuffer()).getData();
        byte[] pixels_neg = ((DataBufferByte) imagem_ppm_negativo.getRaster().getDataBuffer()).getData();
        
        byte[] pix = imagemOriginal.getPixels();
        System.out.println("pegou pixels\n"); 
        while(count < (imagemOriginal.getHeight() * imagemOriginal.getWidth())*3) {
            pixels[count] = (byte) pix[count];
            pixels_neg[count] = (byte) (imagemOriginal.getMaxVal() - pix[count]); 
            count++;  
	}
        System.out.println("Saindo do loop!\n"); 
        exibeImagem = new JFrame();
        exibeImagem.getContentPane().setLayout(new GridLayout(2,1));
        exibeImagem.getContentPane().add(new JLabel(new ImageIcon(imagem_ppm)));
        exibeImagem.getContentPane().add(new JLabel(new ImageIcon(imagem_ppm_negativo)));
        exibeImagem.setMaximumSize(new Dimension(829,1000));
        exibeImagem.pack();
        exibeImagem.setVisible(true);
    }//GEN-LAST:event_jbNegativoActionPerformed

    private void jbPesquisarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbPesquisarActionPerformed
        try {
            FileInputStream arquivo = new FileInputStream(tfendereco.getText());
            BufferedImage imagem_ppm = null;
            int count = 0, count2 = 0;
            byte bbblue, bbred, bbgreen;
            
            String linha = le_linha(arquivo);
            if ("P6".equals(linha)) {
                linha = le_linha(arquivo);
                while (linha.startsWith("#")) {
                    linha = le_linha(arquivo);
                }
                Scanner in = new Scanner(linha);
                if (in.hasNext() && in.hasNextInt()) {
                    imagemOriginal.setWidth(in.nextInt());
                } else {
                    System.out.println("Arquivo corrompido");
                }
                if (in.hasNext() && in.hasNextInt()) {
                    imagemOriginal.setHeight(in.nextInt());
                } else {
                    System.out.println("Arquivo corrompido");
                }
                linha = le_linha(arquivo);
                in.close();
                in = new Scanner(linha);
                imagemOriginal.setMaxVal(in.nextInt());
                in.close();

                imagem_ppm = new BufferedImage(imagemOriginal.getWidth(), imagemOriginal.getHeight(), BufferedImage.TYPE_3BYTE_BGR);

                byte[] pixels = ((DataBufferByte) imagem_ppm.getRaster().getDataBuffer()).getData();
                
                System.out.println("Entrando no loop...\n");                
                while (count < (imagemOriginal.getHeight() * imagemOriginal.getWidth())) {
                    bbred = (byte) arquivo.read();
                    bbgreen = (byte) arquivo.read();
                    bbblue = (byte) arquivo.read();
                    pixels[count2] = bbblue;
                    count2++;
                    pixels[count2] = bbgreen;
                    count2++;
                    pixels[count2] = bbred;
                    count2++;
                    count++;
                }
                imagemOriginal.setPixels(pixels);
                System.out.println("Saindo do loop!\n");                
            } else {
                System.out.println("Arquivo inválido");
            }
            System.out.println("Imprimindo\n");
            
//            exibeImagem = new JFrame();
//            exibeImagem.getContentPane().setLayout(new FlowLayout());
//            exibeImagem.getContentPane().add(new JLabel(new ImageIcon(imagem_ppm)));
//            exibeImagem.pack();
//            exibeImagem.setVisible(true);
            
            JOptionPane.showMessageDialog(null, "Arquivo encontrado em: \n" + tfendereco.getText());
            jbBLUE.setEnabled(true);
            jbRED.setEnabled(true);
            jbGREEN.setEnabled(true);
            jbNegativo.setEnabled(true);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(PGMReader.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, "Arquivo não encontrado em: \n" + tfendereco.getText());
        } catch (IOException ex) {
            Logger.getLogger(PGMReader.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, "Erro na leitura do arquivo.");
        }
    }//GEN-LAST:event_jbPesquisarActionPerformed

    private void tfenderecoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tfenderecoKeyTyped
        if(evt.getKeyChar() == '\n')
            jbPesquisar.doClick(2);
    }//GEN-LAST:event_tfenderecoKeyTyped


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jbBLUE;
    private javax.swing.JButton jbGREEN;
    private javax.swing.JButton jbNegativo;
    private javax.swing.JButton jbPesquisar;
    private javax.swing.JButton jbRED;
    private javax.swing.JLabel labelFiltrosRGB;
    private javax.swing.JLabel labelInfo;
    private javax.swing.JLabel labelNegativo;
    private javax.swing.JTextField tfendereco;
    // End of variables declaration//GEN-END:variables
}
