package model;

public class ImagemPPM extends Imagem {
    private byte[] pixels;
    
    public void setPixels(byte[] pixels) {
        this.pixels = pixels;
    }

    public byte[] getPixels() {
        return pixels;
    }


}
