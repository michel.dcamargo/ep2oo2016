package model;

public class Imagem {
    private int width = 0;
    private int height = 0;
    private int maxVal = 0;
    
    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public int getMaxVal() {
        return maxVal;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public void setMaxVal(int maxVal) {
        this.maxVal = maxVal;
    }
}
