package model;

public class ImagemPGM extends Imagem{
    private byte [] pixels;
    private int posInicial;
    
    public byte[] getPixels() {
        return pixels;
    }

    public int getPosInicial() {
        return posInicial;
    }

    public void setPixels(byte[] pixels) {
        this.pixels = pixels;
    }

    public void setPosInicial(int posInicial) {
        this.posInicial = posInicial;
    }
}
