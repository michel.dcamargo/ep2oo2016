package controller;

import view.PGMReader;
import view.PPMReader;

public class Principal extends javax.swing.JFrame {

    public Principal() {
        initComponents();
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonPPM = new javax.swing.JButton();
        buttonPGM = new javax.swing.JButton();
        labelInformativo = new javax.swing.JLabel();
        labelApresentacao = new javax.swing.JLabel();
        labelApresentacao2 = new javax.swing.JLabel();
        labelApresentacao3 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("ep2oo2016-MichelCamargo");

        buttonPPM.setText("Imagem PPM");
        buttonPPM.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonPPMActionPerformed(evt);
            }
        });

        buttonPGM.setText("Imagem PGM");
        buttonPGM.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonPGMActionPerformed(evt);
            }
        });

        labelInformativo.setFont(new java.awt.Font("Ubuntu", 1, 15)); // NOI18N
        labelInformativo.setText("Escolha um dos dois tipos de imagem!");

        labelApresentacao.setText("Disciplina: Orientação a Objetos - 1/2016 ");

        labelApresentacao2.setText("Professor: Renato Coral ");

        labelApresentacao3.setText("Aluno: Michel Martins de Camargo");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(45, 45, 45)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(labelApresentacao3)
                    .addComponent(labelApresentacao2)
                    .addComponent(labelApresentacao)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(labelInformativo, javax.swing.GroupLayout.PREFERRED_SIZE, 306, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                            .addComponent(buttonPPM, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(buttonPGM, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(45, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addComponent(labelApresentacao)
                .addGap(4, 4, 4)
                .addComponent(labelApresentacao2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(labelApresentacao3)
                .addGap(18, 18, 18)
                .addComponent(labelInformativo, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(buttonPGM, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(buttonPPM, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(45, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void buttonPPMActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonPPMActionPerformed
        PPMReader editPPM = new PPMReader();
    }//GEN-LAST:event_buttonPPMActionPerformed

    private void buttonPGMActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonPGMActionPerformed
        PGMReader editPGM = new PGMReader();
    }//GEN-LAST:event_buttonPGMActionPerformed

    public static void main(String args[]) {

        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Principal().setVisible(true);
            }
        });
//        PGMReader ler = new PGMReader();
        
//        ler.exibirImagem("/home/michel/Imagens/imgteste/lena.pgm");
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton buttonPGM;
    private javax.swing.JButton buttonPPM;
    private javax.swing.JLabel labelApresentacao;
    private javax.swing.JLabel labelApresentacao2;
    private javax.swing.JLabel labelApresentacao3;
    private javax.swing.JLabel labelInformativo;
    // End of variables declaration//GEN-END:variables
}
